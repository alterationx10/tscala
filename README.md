tscala
=======

A TypeScript library inspired by Scala

## About
Hi. I'm new to TypeScript + publishing on npmjs, and I've borked the initial versions!
`1.0.0` and `1.0.1` were published too early. 

I've rebased the project at `0.0.1-alpha.0`, and plan to move forward from there.


## TODO

Some items to (maybe) implement on Option

*    fold — Apply function on optional value, return default if empty
*    foreach — Apply a procedure on option value
*    collect — Apply partial pattern match on optional value
*    filterNot — An optional value doesn't satisfy predicate
*    forall — Apply predicate on optional value, or true if empty

## Build Notes
`npm run format && npm run lint && npm run build`

### Bump the alpha version
`npm version prerelease --preid=alpha`