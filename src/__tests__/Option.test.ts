import { Option, None, Some } from '..';
import { flatMap } from 'tslint/lib/utils';

describe('Options should...', () => {
  test('... be None when initialized with null', () => {
    // Initialize Option with null
    const leNone = Option.apply<number>(null);
    const someAnswer = Option.apply(42);

    //Make sure it is empty/not defined
    expect(leNone.isEmpty()).toBe(true);
    expect(leNone.isDefined()).toBe(false);
    expect(leNone.nonEmpty()).toBe(false);

    // Make sure it never contains anything
    expect(leNone.exists(_ => true)).toBe(false);
    expect(leNone.exists(_ => false)).toBe(false);

    // Make sure we can't map a None to anything else
    expect(leNone.map(_ => {})).toStrictEqual(new None());

    // Make sure we can't filter a None
    expect(leNone.filter(_ => true)).toStrictEqual(new None());
    expect(leNone.filter(_ => false)).toStrictEqual(new None());

    // Make sure we get the else
    expect(leNone.getOrElse(42)).toBe(42);

    expect(leNone.orElse(someAnswer)).toBe(someAnswer);

    expect(leNone.contains(42)).toBe(false);

    expect(leNone.flatMap(_ => someAnswer).isEmpty()).toBe(true);
    expect(leNone.flatMap(_ => someAnswer)).toStrictEqual(leNone);

    // Make sure we throw an error if we ever call get on None
    expect(() => {
      leNone.get();
    }).toThrowError();
  });

  test('... be Some when initialized with a non-null value', () => {
    const aString = 'string';
    const anUpperCaseString = 'STRING';
    const someString = Option.apply(aString);
    const someOtherString = Option.apply('other');
    expect(someString.isDefined()).toBe(true);
    expect(someString.isEmpty()).toBe(false);
    expect(someString.nonEmpty()).toBe(true);
    expect(someString.get()).toBe(aString);
    expect(someString.map(_ => 4).get()).toBe(4);
    function toUpper(str: string): string {
      return str.toUpperCase();
    }
    expect(someString.map(toUpper).get()).toBe(anUpperCaseString);
    expect(someString.map(_ => _.length).get()).toBe(6);
    expect(someString.exists(_ => _.length === 6)).toBe(true);
    expect(someString.exists(_ => _.length === 4)).toBe(false);

    expect(someString.filter(_ => true).nonEmpty()).toBe(true);
    expect(someString.filter(_ => false)).toStrictEqual(new None());
    expect(someString.filter(_ => false).nonEmpty()).toBe(false);

    // Make sure we get the value in getOrElse, and the else if it had turned None
    expect(someString.getOrElse('orElse')).toBe(aString);
    expect(someString.filter(_ => false).getOrElse('orElse')).toBe('orElse');

    expect(someString.orElse(someOtherString)).toBe(someString);
    expect(someString.contains(aString)).toBe(true);
    expect(someString.contains(anUpperCaseString)).toBe(false);

    const someTwo = Option.apply(2);
    const someFour = Option.apply(4);
    expect(someTwo.flatMap(val => Option.apply(2 + val))).toStrictEqual(someFour);
    expect(someTwo.flatMap(val => someTwo.map(_ => _ + val))).toStrictEqual(someFour);
    expect(someTwo.flatMap(_ => new None())).toStrictEqual(new None());
  });
});
