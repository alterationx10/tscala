import { Option } from './package';

export class None<T> extends Option<T> {
  public isEmpty(): boolean {
    return true;
  }

  public map<U>(f: (value: T) => U): None<U> {
    return new None<U>();
  }

  public isDefined(): boolean {
    return false;
  }

  public exists(f: (value: never) => boolean): boolean {
    return false;
  }

  public get(): never {
    throw new Error('Cannot get value of None');
  }

  public nonEmpty(): boolean {
    return false;
  }

  public filter(f: (value: T) => boolean): None<T> {
    return this;
  }

  public getOrElse<U extends T>(alternative: U): T | U {
    return alternative;
  }

  public orElse<U extends T>(alternative: Option<U>): Option<T> | Option<U> {
    return alternative;
  }

  public contains<U extends T>(element: U): boolean {
    return false;
  }

  public flatMap<U>(f: (value: T) => Option<U>): Option<U> {
    return new None<U>();
  }
}
