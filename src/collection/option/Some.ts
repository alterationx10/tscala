import { None, Option } from './package';

export class Some<T> extends Option<T> {
  constructor(private value: T) {
    super();
  }

  public isEmpty(): boolean {
    return false;
  }

  public map<U>(f: (value: T) => U): Option<U> {
    return Option.apply(f(this.value));
  }

  public isDefined(): boolean {
    return true;
  }

  public exists(f: (value: T) => boolean): boolean {
    return f(this.value);
  }

  public get(): T {
    return this.value;
  }

  public nonEmpty(): boolean {
    return true;
  }

  public filter(f: (value: T) => boolean): Option<T> {
    return f(this.value) ? this : new None();
  }

  public getOrElse<U extends T>(alternative: U): T | U {
    return this.value;
  }

  public orElse<U extends T>(alternative: Option<U>): Option<T> | Option<U> {
    return this;
  }

  public contains<U extends T>(element: U): boolean {
    return this.value === element;
  }

  public flatMap<U>(f: (value: T) => Option<U>): Option<U> {
    return f(this.value);
  }
}
