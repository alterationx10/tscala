import { None, Some } from './package';

export abstract class Option<T> {
  public static apply<T = {}>(value?: null | undefined): None<T>;
  public static apply<T>(value?: T): Some<T>;
  public static apply<T>(value?: T | null | undefined): Option<T> {
    return value ? new Some(value) : new None();
  }

  /**
   * Return value, or throw an Error if empty
   */
  public abstract get(): T;

  /**
   * True if empty
   */
  public abstract isEmpty(): boolean;

  /**
   * True if not empty
   */
  public abstract isDefined(): boolean;

  /**
   * True if not empty
   */
  public abstract nonEmpty(): boolean;

  /**
   * Returns a Some containing the result of applying f to this Option's value if this Option is nonempty.
   * @param f The function to operate on the value
   */
  public abstract map<U>(f: (value: T) => U): Option<U>;

  /**
   * Tests whether the option contains a given value as an element.
   * @param f A function that operates on the value
   */
  public abstract exists(f: (value: T) => boolean): boolean;

  /**
   * Returns this Option if it is nonempty and applying the predicate p to this
   * Option's value returns true. Otherwise, return None.
   * @param f
   */
  public abstract filter(f: (value: T) => boolean): Option<T>;

  /**
   * Returns the option's value if the option is nonempty, otherwise return the alternative.
   * @param alternative
   */
  public abstract getOrElse<U extends T>(alternative: U): T | U;

  /**
   * Returns this Option if it is nonempty, otherwise return the result of evaluating alternative.
   * @param alternative
   */
  public abstract orElse<U extends T>(alternative: Option<U>): Option<T> | Option<U>;

  /**
   * Tests whether the option contains a given value as an element.
   * @param element
   */
  public abstract contains<U extends T>(element: U): boolean;

  /**
   * Returns the result of applying f to this Option's value if this Option is nonempty.
   * Returns None if this Option is empty.
   * Slightly different from map in that f is expected to return an Option (which could be None).
   * @param f
   */
  public abstract flatMap<U>(f: (value: T) => Option<U>): Option<U>;
}
